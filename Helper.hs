{-# LANGUAGE DoAndIfThenElse #-}
module Helper where
import qualified SDL
import qualified SDL.Image as Image
import qualified SDL.Font as Font
import Foreign.C.Types(CInt)
import Debug.Trace(trace)
windowWidth = 1280
windowHeight = 720

type Position = SDL.V2 Double
type Velocity = SDL.V2 Double

data SDLContext = SDLContext
    { sdlwindow   :: !SDL.Window
    , sdlrenderer :: !SDL.Renderer
    , sdlWindowSurface :: !SDL.Surface
    , sdlscreen   :: !SDL.Surface
    }

data LoadedGraphics = LoadedGraphics
    { personSpriteLeft  :: !SDL.Surface
    , personSpriteRight :: !SDL.Surface
    , particleSprite    :: !SDL.Surface
    , fireSprites       :: ![SDL.Surface]
    , overlay           :: !OverlayGraphics
    , dotrice           :: !Font.Font
    }

data OverlayGraphics = OverlayGraphics
    { background      :: !SDL.Surface
    , ground          :: !SDL.Surface
    , clouds          :: !SDL.Surface
    , shadow          :: !SDL.Surface
    , lights          :: !SDL.Surface
    , blockers        :: !SDL.Surface
    , backgroundFlare :: !SDL.Surface
    , popscreen       :: !SDL.Surface
    , popstation      :: !SDL.Surface
    , foregroundFlare :: !SDL.Surface
    , titleScreen     :: !SDL.Surface
    }

randomV :: SDL.V2 Double -> SDL.V2 Double
randomV (SDL.V2 x y) = SDL.V2
    (random y)
    (random x)

randomChoice :: [a] -> Double -> a
randomChoice list seed = list !! randomInt (length list) seed

randomInt :: Int -> Double -> Int
randomInt maxInt seed = floor $ random seed * (fromIntegral maxInt)

-- provided with a sufficiently random input, returns a value 0-1
random :: Double -> Double
random x = decimal $ x*1000 + 0.684290100 -- phone number to a local Domino's

decimal :: Double -> Double
decimal n = n - (fromIntegral $ floor n :: Double)

rotateV :: (Floating a) => a -> SDL.V2 a -> SDL.V2 a
rotateV angle (SDL.V2 x y) = SDL.V2
    (cos angle * x - sin angle * y)
    (sin angle * x + cos angle * y)

dotV (SDL.V2 x1 y1) (SDL.V2 x2 y2) = (x1*x2) + (y1*y2)

-- the angle you need to rotate "a" by to get "b"
angleV :: (RealFloat a) => SDL.V2 a -> SDL.V2 a -> a
angleV b@(SDL.V2 x y) a =
    -- https://straypixels.net/angle-between-vectors/
    let p = SDL.V2 (-y) x
        bCoord = dotV a b
        pCoord = dotV a p in
    atan2 pCoord bCoord

optimizedLoad :: SDL.Surface -> FilePath -> IO SDL.Surface
optimizedLoad sscreen path = do
    image <- (Image.load path) :: IO SDL.Surface
    --SDL.surfaceBlendMode image SDL.$= SDL.BlendNone
    screenFormat <- SDL.surfaceFormat sscreen
    optimizedImage <- SDL.convertSurface image screenFormat
    return optimizedImage

drawImage sprite screen pos =
    SDL.surfaceBlit sprite Nothing screen (Just $ SDL.P $ pos)

eulerLength :: (Floating a) => SDL.V2 a -> a
eulerLength (SDL.V2 a b) = (a ** 2 + b ** 2) ** 0.5

(*|) c = fmap (*c)

countIfTrue :: [Bool] -> Int
countIfTrue = foldl (flip ((+) . fromEnum)) 0


screenPosition :: Position -> SDL.V2 CInt
screenPosition (SDL.V2 p1 p2) = SDL.V2 (round p1) (round p2)

toGrid :: (Integral a) => a -> Position -> SDL.V2 CInt
toGrid gridSize (SDL.V2 p1 p2) = SDL.V2 (g p1) (g p2)
  where
    g :: Double -> CInt
    g p = (round p) `div` gridCInt * gridCInt
    gridCInt = (fromIntegral gridSize) :: CInt

inRect :: (Ord a, Num a) => SDL.V2 a -> SDL.Rectangle a ->  Bool
inRect (SDL.V2 px py) (SDL.Rectangle (SDL.P (SDL.V2 rx ry)) (SDL.V2 w h)) =
    px >= rx && py >= ry && px <= (rx+w) && py <= (ry+h)

intersectingR :: (Ord a, Num a) => SDL.Rectangle a -> SDL.Rectangle a -> Bool
intersectingR (SDL.Rectangle (SDL.P (SDL.V2 rx ry)) (SDL.V2 w h)) otherRect =
    i rx ry || i rx (ry+h) || i (rx+w) ry || i (rx+w) (ry+h)
  where i x y = SDL.V2 x y `inRect` otherRect

cornersR :: (Num a) => SDL.Rectangle a -> [SDL.V2 a]
cornersR (SDL.Rectangle (SDL.P (SDL.V2 rx ry)) (SDL.V2 w h)) =
    [v rx ry , v rx (ry+h) , v (rx+w) ry , v (rx+w) (ry+h)]
  where v=SDL.V2


containedInR :: (Ord a, Num a) => SDL.Rectangle a -> SDL.Rectangle a -> Bool
containedInR (SDL.Rectangle (SDL.P (SDL.V2 rx ry)) (SDL.V2 w h)) otherRect =
    i rx ry && i rx (ry+h) && i (rx+w) ry && i (rx+w) (ry+h)
  where i x y = SDL.V2 x y `inRect` otherRect

rem_ x y = x - (y * (fromIntegral $ truncate (x/y)))

area (SDL.Rectangle _ (SDL.V2 w h)) = w*h

widthR (SDL.Rectangle _ (SDL.V2 w _)) = w
heightR (SDL.Rectangle _ (SDL.V2 _ h)) = h

toIntR :: SDL.Rectangle Double -> SDL.Rectangle Int
toIntR (SDL.Rectangle (SDL.P (SDL.V2 rx ry)) (SDL.V2 w h)) =
    SDL.Rectangle (SDL.P (SDL.V2 rx_ ry_)) (SDL.V2 w_ h_)
  where rx_ = round rx
        ry_ = round ry
        w_ = round w
        h_ = round h

toDoubleR :: SDL.Rectangle Int -> SDL.Rectangle Double
toDoubleR (SDL.Rectangle (SDL.P (SDL.V2 rx ry)) (SDL.V2 w h)) =
    SDL.Rectangle (SDL.P (SDL.V2 rx_ ry_)) (SDL.V2 w_ h_)
  where rx_ = fromIntegral rx
        ry_ = fromIntegral ry
        w_ = fromIntegral w
        h_ = fromIntegral h

intersectR :: (Ord a, Num a) => SDL.Rectangle a -> SDL.Rectangle a -> SDL.Rectangle a
intersectR
    (SDL.Rectangle (SDL.P (SDL.V2 px py)) (SDL.V2 pw ph))
    (SDL.Rectangle (SDL.P (SDL.V2 qx qy)) (SDL.V2 qw qh))
    = SDL.Rectangle
        (SDL.P $ SDL.V2 (max px qx) (max py qy))
        (SDL.V2 (abs $ pw-px-qw+qx) (abs $ ph-py-qh+qy))
{-# LANGUAGE DoAndIfThenElse #-}
module UIData where
import Helper(inRect)
import SDL(Rectangle(Rectangle), Point(P), V2(V2))

data Buttons = StartGame | MuteMusic | MuteSound | NoButton deriving Eq

getButton mpos
    | mpos `inRect` startRect = StartGame
    | mpos `inRect` musicRect = MuteMusic
    | mpos `inRect` sfxRect   = MuteSound
    | otherwise               = NoButton

startRect = rect 490 390 300 64
musicRect = rect 965 174 56 23
sfxRect = rect 956 206 0 0

rect x y w h = Rectangle (P $ V2 x y) (V2 w h)
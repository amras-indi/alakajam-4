{-# LANGUAGE DoAndIfThenElse #-}
module Person (step, draw, PersonState, defaultPersonState, particles, alive,
    Particle) where

import qualified SDL
import SDL(V2(..))
import qualified Control.Monad.State as S
import Foreign.C.Types(CInt)
import Linear.Affine(Point(P))
import qualified Data.HashSet as HS
import Data.Maybe(fromJust, catMaybes)
import Control.Monad(unless)

import Helper(SDLContext(..), LoadedGraphics(..), randomV, random, rotateV, angleV,
    Position, Velocity, eulerLength, (*|), toGrid, inRect, cornersR, intersectR,
    area, widthR, heightR, toDoubleR)
import LevelData(levelRect, levelRectInt, roundToPanel, panelRect, inBounds)

data PersonState = PersonState
    Position
    Velocity
    Bool -- isPushing
    Facing
    [Particle]
    Bool -- alive

particles (PersonState _ _ _ _ ps _) = map parPos ps
alive (PersonState _ _ _ _ _ a) = a

data Particle = Particle
    { parPos :: Position
    , parVel :: Velocity
    , timeToLive :: Double
    } deriving Show

data Facing = FRight | FLeft

defaultPersonState = PersonState (V2 640 500) (V2 0 0) False FRight [] True

max_velocity = 12 :: Double
acceleration = 1.25 :: Double -- constant
drag = 0.25 :: Double -- portion of velocity
bounce_slowdown = 0.5
person_grid = 4 :: CInt
person_offset = V2 (-16) (-32)
personRect pos = SDL.Rectangle (SDL.P $ pos+person_offset) (V2 32.0 64.0)

max_particles = 50 :: Int
max_particle_velocity = 7 :: Double
max_particle_ttl = 1.25 :: Double
particle_grid = 4 :: CInt
particle_offset_left = V2 (-16) (-15)
particle_offset_right = V2 (16) (-15)

step
    :: Double  -- dt
    -> V2 Double -- mousepos
    -> [SDL.Event] -- all events
    -> HS.HashSet (V2 Int) -- active panels
    -> S.State PersonState ()
step dt mousepos@(V2 mx _) events activePanels = do
    PersonState pos vel push _ prts ali <- S.get
    let push_ = if push then not $ any (isMouse SDL.Released) events
                else any (isMouse SDL.Pressed) events
        force = if push_ -- increase or decrease our speed
                then withLength acceleration $ pos - mousepos
                else withLength (drag * eulerLength vel) $ -vel
        vel_ = clampLength max_velocity
             $ vel + (dt *|force)
        pos_@(V2 px _) = pos + vel_
        facing = if px > mx then FLeft else FRight
        particles_ = particleStep dt pos (-force) push_ facing prts activePanels
    S.put $ if inBounds_ pos_
        then PersonState pos_ vel_ push_ facing particles_ ali
        else if inBounds_ (pos + flipH vel_)
        then PersonState (pos + flipH vel_) (flipH vel_) push_ facing particles_ ali
        else if inBounds_ (pos + flipV vel_)
        then PersonState (pos + flipV vel_) (flipV vel_) push_ facing particles_ ali
        else if inBounds_ pos
        then PersonState pos (V2 0.0 0.0) push_ facing particles_ ali
        else if fixBounds activePanels pos /= Nothing
        then PersonState (fromJust $ fixBounds activePanels pos) (V2 0.0 0.0)
                push_ facing particles_ ali
        else PersonState pos (V2 0.0 0.0) False facing particles_ False
  where
    isMouse motion e = -- check if the mouse is currently switching to down/up
      case SDL.eventPayload e of
        SDL.MouseButtonEvent mouseEvent ->
          SDL.mouseButtonEventButton mouseEvent == SDL.ButtonLeft
          && SDL.mouseButtonEventMotion mouseEvent == motion
        _ -> False
    inBounds_ pos = all (inBounds activePanels) $ cornersR $ personRect pos
    flipV (V2 x y) = if y<0 then bounce_slowdown *| V2 x (-y) else V2 0 0
    flipH (V2 x y) = bounce_slowdown *| V2 (-x) y

collidesWith :: HS.HashSet (V2 Int) -- activePanels
    -> Position -> Maybe (SDL.Rectangle Int)
collidesWith panels pos =
    if not $ roundToPanel pos `HS.member` panels then Just $ panelRect $ roundToPanel pos
    else if not $ pos `inRect` levelRect then Just levelRectInt
    else Nothing

fixBounds :: HS.HashSet (V2 Int) -- activePanels
    -> Position -> Maybe Position
fixBounds panels pos =
    foldl (\pos_ rect ->
        pos_ >>= fixBoundsFor rect
        ) (Just pos) $ collidesWith_
  where
    inBounds_ p = all (inBounds panels) $ cornersR $ personRect p
    collidesWith_ =
        map toDoubleR $ catMaybes $
        map (collidesWith panels) $ cornersR $ personRect pos
    fixBoundsFor rect p
        | (area $ intersection rect p) > 0.5 * (area $ personRect p) = Nothing
        | inBounds_ (p+V2 w 0) = Just (p+V2 w 0)
        | inBounds_ (p-V2 w 0) = Just (p-V2 w 0)
        | inBounds_ (p+V2 0 h) = Just (p+V2 0 h)
        | inBounds_ (p-V2 0 h) = Just (p-V2 0 h)
        | otherwise = Nothing
      where w = widthR rect
            h = heightR rect

    intersection rect p = intersectR rect $ personRect p

particleStep
    :: Double
    -> Position
    -> Velocity
    -> Bool  -- generate a new particle
    -> Facing  -- whether to generate on the left or the right
    -> [Particle] -- existing particles
    -> HS.HashSet(V2 Int) -- active panels
    -> [Particle]
particleStep _ _ _ False _ [] _ = []
-- generate a new particle
particleStep dt pos@(V2 x y) direction True facing [] _ = [Particle
    { parPos = pos + case facing of
                       FLeft -> particle_offset_left
                       FRight -> particle_offset_right
    , parVel = rotateV angleVel localVel
    , timeToLive = max_particle_ttl * (random $ x+y)
    }]
  where localVel = max_particle_velocity *|(randomV $ (1/dt)*|pos)
        angleVel = angleV (V2 1 1) direction

-- update existing particles
particleStep dt pos dir pushing facing (particle:ps) activePanels
    | timeToLive particle <= dt =  rest
    | outOfBounds = rest
    | otherwise = Particle -- update each particle
        { parPos = parPos particle + parVel particle
        , parVel = parVel particle
        , timeToLive = timeToLive particle - dt
        } : rest
  where rest = particleStep dt pos dir keepSpawning facing ps activePanels
        keepSpawning = pushing && length ps < max_particles
        newPos = parPos particle + parVel particle
        outOfBounds = not $ inBounds activePanels newPos


draw :: SDLContext -> LoadedGraphics -> PersonState -> IO ()
draw sdl sprites (PersonState pos vel push facing particles_ ali) = do
    let screenPos = P $ toGrid person_grid $ pos + person_offset
    unless (not ali) $ do
        SDL.surfaceBlit perSprite Nothing screen (Just $ screenPos)
        drawParticles sdl parSprite particles_
        return ()
  where
    screen = sdlscreen sdl
    perSprite = case facing of
        FLeft -> personSpriteLeft sprites
        FRight -> personSpriteRight sprites
    parSprite = particleSprite sprites

drawParticles :: SDLContext -> SDL.Surface -> [Particle] -> IO()
drawParticles _ _ [] = return ()
drawParticles sdl image (particle:ps) = do
    let screenPos = P $ toGrid particle_grid $ parPos particle

    SDL.surfaceBlit image Nothing screen (Just $ screenPos)
    drawParticles sdl image ps
    return ()
  where
    screen = sdlscreen sdl

withLength :: (Floating a, SDL.Epsilon a) => a -> V2 a -> V2 a
withLength l vec = l *| (SDL.normalize $ vec)

clampLength :: (Ord a, Floating a, SDL.Epsilon a) => a -> V2 a -> V2 a
clampLength l vec
    | eulerLength vec <= l = vec
    | otherwise = withLength l vec
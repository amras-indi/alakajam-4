{-# LANGUAGE DoAndIfThenElse #-}
module Music(MusicCache(..), initialize, loop, stop, toggle, verify) where
import qualified SDL.Mixer as Mix
import Control.Monad(unless)


data MusicCache = MusicCache
    { main :: Mix.Music
    , title :: Mix.Music
    , muted :: Bool
    }

initialize :: IO (MusicCache)
initialize = do
    let audio = Mix.Audio 22050 Mix.FormatS16_Sys Mix.Stereo
    Mix.openAudio audio 4096
    main_ <- Mix.load "sfx/main.ogg"
    title_ <- Mix.load "sfx/title.ogg"
    return MusicCache
        { main = main_
        , title = title_
        , muted = False
        }

loop :: Mix.Music -> IO ()
loop = Mix.playMusic Mix.Forever

toggle :: IO()
toggle = Mix.pausedMusic >>= \p -> if p then Mix.resumeMusic else Mix.pauseMusic

verify :: MusicCache  -> IO()
verify cache = do
    paused <- Mix.pausedMusic
    unless (muted cache == paused) toggle

stop :: IO()
stop = Mix.haltMusic

{-# LANGUAGE DoAndIfThenElse #-}
module LevelData where
import SDL(V2(..), Rectangle(..), Point(P))
import Data.Maybe(fromJust)
import qualified Data.HashSet as HS
import Data.HashSet(HashSet(..))

import Helper(Position, inRect)

panelSize = 40 :: Int

firePositions :: HashSet (V2 Int) -> HashSet (V2 Double)
firePositions activePanels =
    HS.map firePos $ HS.difference
    (groundPanels `HS.union` (catMaybes_ $ HS.map above decayPanels))
    decayPanels
  where
    decayPanels = allPanels `HS.difference` activePanels
    firePos panel@(V2 x y) = V2 ((fromIntegral x)+(s/2)) (fromIntegral (y+panelSize))
    above panel@(V2 x y) = if y <= 160 then Nothing
        else Just $ V2 x (y-panelSize)
    s = (fromIntegral panelSize)
    catMaybes_ = (HS.map fromJust) . (HS.filter (/=Nothing))

groundPanels :: HashSet (V2 Int)
groundPanels = HS.fromList $ map (\x -> V2 x (560-panelSize))
    [240, 240+panelSize..1040-panelSize]

groundFirePositions :: HashSet (V2 Double)
groundFirePositions = HS.fromList $
    map (\x -> V2 x 560.0) [240+(s/2),240+(3*s/2)..1040-(s/2)]
  where s = (fromIntegral panelSize)

levelRect = Rectangle (P $ V2 240.0 160.0) (V2 800 400)
levelRectInt = Rectangle (P $ V2 240 (160::Int)) (V2 800 400)
levelColliders = [] :: [Rectangle Double]

allPanels :: HashSet (V2 Int)
allPanels = HS.fromList $ flatten $ map (\y ->
                    map (\x -> V2 (x::Int) (y::Int))
                    [240, 240+panelSize..1040-panelSize]
                ) [160, 160+panelSize..560-panelSize]

panelRect :: V2 Int -> SDL.Rectangle Int
panelRect p@(V2 x y) = SDL.Rectangle (SDL.P p) (V2 panelSize panelSize)

roundToPanel :: (RealFrac a) => V2 a -> V2 Int
roundToPanel (V2 x y) = V2
    (((fromIntegral $ floor x - xOffset) `div` panelSize) * panelSize + xOffset)
    (((fromIntegral $ floor y - yOffset) `div` panelSize) * panelSize + yOffset)
  where
    xOffset = 240 `rem` panelSize
    yOffset = 160 `rem` panelSize

flatten [] = []
flatten (x:xs) = x++flatten xs

rem_ x y = x - (y * (fromIntegral $ truncate (x/y)))

inBounds :: HS.HashSet (SDL.V2 Int) -- active panels
     -> Position -> Bool
inBounds panels pos =
    roundToPanel pos `HS.member` panels
    && pos `inRect` levelRect
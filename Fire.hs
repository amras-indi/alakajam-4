{-# LANGUAGE DoAndIfThenElse #-}
module Fire(draw, step, FireState(..), Fire(..), defaultFireState, max_hp
    , fire_hitbox_offset) where

import qualified SDL
import qualified Control.Monad.State as S
import qualified Data.HashSet as HS
import Data.Hashable(Hashable(..))
import Foreign.C.Types(CInt)
import Data.Maybe(fromJust)
--import Debug.Trace(trace)

import Helper(SDLContext(..), Position, drawImage, LoadedGraphics(fireSprites), eulerLength
        , countIfTrue, screenPosition, randomChoice, inRect)
import LevelData(panelSize, levelRect)

data FireState = FireState
    { fsFires         :: HS.HashSet(Fire)
    , fsTimeUntilFire :: Double
    , score           :: Int
    }
data Fire = Fire
    { firePos :: Position
    , fireHP  :: Int
    , fireAge :: Double
    } deriving (Eq)

instance Hashable Fire where
    hashWithSalt s = hashWithSalt s.firePos

defaultFireState = FireState HS.empty 3 0
default_hp = 4
max_hp = 12
recover_time = 3 :: Double

fire_draw_offset = SDL.V2 (-16::CInt) (-48::CInt)
fire_hitbox_offset = SDL.V2 (0::Double) (-fire_radius)
fire_radius = 16 :: Double
max_fires = 16 :: Int
fire_time = 3 :: Double

step ::
    Double        -- dt, frame length
    -> HS.HashSet(Position) -- spots for fire
    -> [Position] -- particles
    -> S.State FireState ()
step dt openPositions particles = do
    FireState fires tuf scor <- S.get
    let openPos = openPositions `HS.difference` HS.map firePos fires
        timeUntilFire = tuf-dt
        newFirePos = if timeUntilFire > 0 || length fires > max_fires
                     then Nothing
                     else generateFirePos (HS.toList openPos) timeUntilFire
        newFire = fmap (\pos -> Fire pos default_hp 0) newFirePos
        updatedFires = map (stepFire particles) $ HS.toList fires
    S.put $ FireState
        { fsFires = catMaybes_ $ newFire `HS.insert` HS.fromList updatedFires
        , fsTimeUntilFire = if timeUntilFire < 0
                            then fire_time + timeUntilFire
                            else timeUntilFire
        , score = scor + (length $ filter (==Nothing) updatedFires)
        }
  where
    catMaybes_ = (HS.map fromJust) . (HS.filter (/=Nothing))
    stepFire :: [Position] -> Fire -> Maybe Fire
    stepFire prts fire
        | hp <= 0 = Nothing
        | outOfBounds = if onScreen nextPos
            then Just fire {fireHP = default_hp, fireAge = 0, firePos = nextPos}
            else Nothing
        | otherwise = Just fire {fireHP = hp, fireAge = age}
        where
          age_ = fireAge fire + dt
          age = if age_ > recover_time then age_ - recover_time else age_
          hp = fireHP fire
                - (countIfTrue $ map (intersects fire) prts)
                + (if age_ > recover_time
                   then 1 else 0)
          nextPos = firePos fire - SDL.V2 0 (fromIntegral panelSize)
          outOfBounds = not $ firePos fire `HS.member` openPositions
          onScreen pos = pos `inRect` levelRect
    intersects :: Fire -> Position -> Bool
    intersects fire particle =
        eulerLength (firePos fire + fire_hitbox_offset - particle) < fire_radius

generateFirePos :: [Position] -> Double -> Maybe Position
generateFirePos [] _ = Nothing
generateFirePos [p] _ = Just p
generateFirePos ps seed = Just $ randomChoice ps seed

draw :: SDLContext -> LoadedGraphics -> FireState -> IO ()
draw sdl lg firestate = drawFires sdl lg (HS.toList $ fsFires firestate)

drawFires _ _ [] = return ()
drawFires sdl images (fire:fs) = do
    let hp = fireHP fire
        seed = (round $ fireAge fire) + hp
        sprite = sprites !! if hp >= max_hp-1 then 4 else if hp == max_hp-2 then 3 else seed `mod` 3
    drawImage sprite (sdlscreen sdl) (fire_draw_offset + (screenPosition $ firePos fire))
    drawFires sdl images fs
  where
    sprites = fireSprites images
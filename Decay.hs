{-# LANGUAGE DoAndIfThenElse #-}
module Decay(step, draw, DecayState(..), defaultDecayState) where
import qualified SDL
import qualified Control.Monad.State as S
import qualified Data.HashSet as HS
import Linear (V4(..))
import Foreign.C.Types(CInt)
import SDL(Rectangle(..), V2(..), ($=), Point(P))

import LevelData(panelSize, allPanels, roundToPanel)
import Helper(SDLContext(..))
import Fire(FireState(fsFires), Fire(..), max_hp, fire_hitbox_offset)

data DecayState = DecayState
    { activePanels :: HS.HashSet(SDL.V2 Int) -- todo: hashSet?
    }
defaultDecayState = DecayState allPanels

step :: FireState -> S.State DecayState ()
step fireState = do
    DecayState panels <- S.get
    let decayedPanels =
            HS.map roundToPanel
            $ HS.map ((+fire_hitbox_offset) . firePos)
            $ HS.filter (\f -> fireHP f >= (fromIntegral max_hp))
            $ fsFires fireState
    S.put DecayState
        { activePanels = panels `HS.difference` decayedPanels
        }
    return ()


draw :: SDLContext -> DecayState -> IO ()
draw sdl decayState = do
    drawPanels $ HS.toList $ activePanels decayState
  where
    drawPanels [] = return ()
    drawPanels (V2 x y:ps) =
        drawPanel sdl x y >> drawPanels ps

drawPanel :: SDLContext -> Int -> Int -> IO ()
drawPanel sdl x_ y_ = do
    SDL.rendererDrawColor renderer $= V4 85 100 47 255
    SDL.fillRect renderer $ Just $ Rectangle
     (P$V2 (x::CInt) (y::CInt))
     (V2 (pSize::CInt) (pSize::CInt))
   where
    renderer = sdlrenderer sdl
    pSize = (fromIntegral panelSize)
    x = fromIntegral x_
    y = fromIntegral y_

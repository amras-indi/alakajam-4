{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DoAndIfThenElse #-}

module Main(main) where

import qualified SDL
import qualified SDL.Font as Font
import qualified SDL.Raw.Types as Raw
import qualified Control.Monad.State as S
import qualified Data.Time as Time
import Control.Monad(unless)
import SDL(($=), V2(..))
import Foreign.C.Types(CInt)
import Text.Printf(printf)
import Data.Text(pack)
import Debug.Trace(trace)

import qualified Person
import Helper(SDLContext(..), LoadedGraphics(..), optimizedLoad)
import qualified Overlay
import qualified Fire
import qualified Decay
import qualified LevelData
import qualified Music
import qualified UIData

data GameState = GameState
    { personState   :: Person.PersonState
    , fireState     :: Fire.FireState
    , decayState    :: Decay.DecayState
    , lastFrameTime :: Time.UTCTime
    }

startGameState :: Time.UTCTime -> GameState
startGameState = GameState
    Person.defaultPersonState
    Fire.defaultFireState
    Decay.defaultDecayState

main :: IO ()
main = do
    sdl <- initializeSDL
    sprites <- initializeSprites sdl
    music <- Music.initialize
    Music.loop (Music.title music)
    titleloop sdl sprites music
    return ()

initializeSDL :: IO SDLContext
initializeSDL = do
    SDL.initializeAll
    Font.initialize
    let rconf = SDL.RendererConfig
            { SDL.rendererType = SDL.SoftwareRenderer
            , SDL.rendererTargetTexture = False
            }
        size = V2 (1280::CInt) (720::CInt)
    window_ <- SDL.createWindow "Fire! Fire!" SDL.defaultWindow
    SDL.windowSize window_ $= size
    renderer_ <- SDL.createRenderer window_ (-1) rconf
    screen_ <- SDL.getWindowSurface window_
    intermediate <- SDL.createRGBSurface size SDL.RGBA8888

    return $ SDLContext window_ renderer_ screen_ intermediate

initializeSprites :: SDLContext -> IO LoadedGraphics
initializeSprites sdl = do
    personleftpng <- load "person_left.png"
    personrightpng <- load "person_right.png"
    particlepng <- load "particle.png"
    overlay_ <- Overlay.initialize sscreen
    f1 <- load "fire/1.png"
    f2 <- load "fire/2.png"
    f3 <- load "fire/3.png"
    f9 <- load "fire/9.png"
    f10 <- load "fire/10.png"
    font <- Font.load "Dotrice-Regular.otf" 64
    return LoadedGraphics
        { personSpriteLeft = personleftpng
        , personSpriteRight = personrightpng
        , particleSprite = particlepng
        , fireSprites = [f1,f2,f3, f9, f10]
        , overlay = overlay_
        , dotrice = font
        }
 where
   sscreen = sdlscreen sdl
   load = optimizedLoad sscreen

titleloop :: SDLContext -> LoadedGraphics -> Music.MusicCache -> IO()
titleloop sdl sprites music = do
    SDL.delay 15
    absMousePos <- SDL.getAbsoluteMouseLocation
    events <- SDL.pollEvents
    Music.verify music
    let mouseClicked = any isMouseClick events
        button = if mouseClicked then UIData.getButton (intMouse absMousePos)
                 else UIData.NoButton
        musicRequested = button == UIData.MuteMusic
        muted_ = if musicRequested then not $ Music.muted music else Music.muted music
    Overlay.drawTitle sdl (overlay sprites)
    
    SDL.surfaceBlit screen Nothing windowSurface Nothing
    SDL.updateWindowSurface window
    SDL.present renderer
    if button == UIData.StartGame then do
            time <- Time.getCurrentTime
            Music.loop (Music.main music)
            Music.verify music
            mainloop sdl sprites (startGameState time)
            Music.loop (Music.title music)
            else return()
    unless (any isCloseEvent events) $ titleloop sdl sprites music{Music.muted=muted_}
    SDL.quit
  where
    intMouse (SDL.P (V2 x y)) =
        V2 ((fromIntegral x)::Integer) ((fromIntegral y)::Integer)
    window = sdlwindow sdl
    renderer = sdlrenderer sdl
    screen = sdlscreen sdl
    windowSurface = sdlWindowSurface sdl


mainloop :: SDLContext -> LoadedGraphics -> GameState -> IO ()
mainloop sdl sprites gs = do
    dt <- timeSince $ lastFrameTime gs
    absMousePos <- SDL.getAbsoluteMouseLocation
    let mousePos = naturalMouse absMousePos
    events <- SDL.pollEvents

    let newgs = S.execState (step dt mousePos events) gs

    draw sdl sprites newgs

    unless (any isCloseEvent events) $ mainloop sdl sprites newgs
  where naturalMouse (SDL.P (V2 x y)) =
            V2 ((fromIntegral x)::Double) ((fromIntegral y)::Double)
        timeSince start = Time.getCurrentTime
            >>= return . realToFrac . flip Time.diffUTCTime start

isCloseEvent event = case SDL.eventPayload event of
    SDL.WindowClosedEvent _ -> True
    SDL.KeyboardEvent keyboardEvent ->
        SDL.keyboardEventKeyMotion keyboardEvent == SDL.Pressed
        && (not $ SDL.keyboardEventRepeat keyboardEvent)
        && (SDL.keysymKeycode $ SDL.keyboardEventKeysym keyboardEvent) ==
            SDL.KeycodeEscape
    _ -> False

isMouseClick event = case SDL.eventPayload event of
    SDL.MouseButtonEvent mouseEvent ->
           SDL.mouseButtonEventButton mouseEvent == SDL.ButtonLeft
        && SDL.mouseButtonEventMotion mouseEvent == SDL.Pressed
    _ -> False

step :: Double -> V2 Double -> [SDL.Event] -> S.State GameState ()
step dt mousePos events = do
    gs <- S.get
    let dState = S.execState (Decay.step (fireState gs)) $ decayState gs
        _aPanels = Decay.activePanels dState
        pState = S.execState (Person.step dt mousePos events _aPanels) $ personState gs
        _oPositions = LevelData.firePositions _aPanels
        fState = S.execState (
            Fire.step dt _oPositions (Person.particles pState)) $ fireState gs
        newTime = Time.addUTCTime (realToFrac dt) $ lastFrameTime gs
    S.put $ GameState pState fState dState newTime

draw :: SDLContext -> LoadedGraphics -> GameState -> IO ()
draw sdl sprites gs = do
    -- background
    Overlay.drawBackground sdl (overlay sprites)
    SDL.surfaceBlit screen Nothing windowSurface Nothing
    SDL.surfaceFillRect screen Nothing (SDL.V4 0 0 0 0)

    -- gameplay
    Decay.draw sdl (decayState gs)
    Person.draw sdl sprites (personState gs)
    Fire.draw sdl sprites (fireState gs)

    drawScore $ Fire.score $ fireState gs
    unless (Person.alive $ personState gs) drawGameOver

    -- foreground
    Overlay.drawForeground sdl (overlay sprites)

    -- commit changes to the screen
    SDL.surfaceBlit screen Nothing windowSurface Nothing
    SDL.updateWindowSurface window
    SDL.present renderer
  where
    window = sdlwindow sdl
    renderer = sdlrenderer sdl
    screen = sdlscreen sdl
    windowSurface = sdlWindowSurface sdl
    drawGameOver = drawText 450 200 "GAME OVER" >> drawText 325 300 "Esc to restart"
    drawScore score = drawText 250 175 $ pack $ printf "%04d" score
    drawText x y text = do
        let color = SDL.V4 0 0 0 255
        t <- Font.blended (dotrice sprites) color text
        let textpos = SDL.P$V2 x y
        SDL.surfaceBlit t Nothing screen (Just textpos)
        return ()


{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DoAndIfThenElse #-}
module Overlay(drawBackground, drawForeground, drawTitle, initialize) where

import qualified SDL
import Helper(SDLContext(..), OverlayGraphics(..), windowWidth, windowHeight,
    optimizedLoad, drawImage, rem_)
import Data.Time.Clock.POSIX (getPOSIXTime)
import Foreign.C.Types(CInt)
--import Debug.Trace(trace)

initialize :: SDL.Surface -> IO OverlayGraphics
initialize screen = do
    _background      <- load "overlay/1background.png"
    _ground          <- load "overlay/2ground.png"
    _clouds          <- load "overlay/3clouds.png"
    _shadow          <- load "overlay/4shadow.png"
    _lights          <- load "overlay/5lights.png"
    _blockers        <- load "overlay/6blockers.png"
    _backgroundFlare <- load "overlay/7backgroundflare.png"
    _screen          <- load "overlay/8screen.png"
    _popstation      <- load "overlay/9popstation.png"
    _foregroundFlare <- load "overlay/Aforegroundflare.png"
    _titleScreen     <- load "overlay/Btitlescreen.png"
    return $ OverlayGraphics
        _background _ground _clouds _shadow _lights _blockers
        _backgroundFlare _screen _popstation _foregroundFlare _titleScreen
  where
    load = optimizedLoad screen


drawEarth sdl images t = do
    drawImage  (background images) sscreen origin
    doubleDraw (ground images)     sscreen (groundPos t)
    doubleDraw (clouds images)     sscreen (cloudPos t)
    drawImage  (shadow images)     sscreen origin
    doubleDraw (lights images)     sscreen (groundPos t)
    drawImage  (blockers images)   sscreen origin
    return ()
  where
    sscreen = sdlscreen sdl
    groundPos = tPos (-25)
    cloudPos = tPos (-100)

drawTitle :: SDLContext -> OverlayGraphics -> IO()
drawTitle sdl images = do
    t <- getT
    drawEarth sdl images t
    drawImage (titleScreen images) sscreen origin
    drawImage  (popstation images) sscreen origin
    return ()
  where
    sscreen = sdlscreen sdl


drawBackground :: SDLContext -> OverlayGraphics ->  IO()
drawBackground sdl images = do
    t <- getT
    drawEarth sdl images t
    doubleDrawDiag (backgroundFlare images) sscreen (flarePos t)
    drawImage (popscreen images) sscreen origin
    return ()
  where
    sscreen = sdlscreen sdl
    flarePos = tDiagPos (-300) (-300)


drawForeground :: SDLContext -> OverlayGraphics -> IO()
drawForeground sdl images = do
    time <- getPOSIXTime
    let t = rem_ time (fromIntegral windowWidth)
    drawImage  (popstation images) sscreen origin
    doubleDrawDiag (foregroundFlare images) sscreen (flarePos t)
    return ()
  where
    sscreen = sdlscreen sdl
    flarePos = tDiagPos (-500) (-500)


getT = getPOSIXTime >>=
    \time -> return $ rem_ time (fromIntegral windowWidth)

origin = SDL.V2 (0::CInt) (0::CInt)
tDiagPos scaleX scaleY t = SDL.V2
    (tLerp scaleX t windowWidth)
    (tLerp scaleY t windowHeight)

tPos scale t = SDL.V2
    (tLerp scale t windowWidth)
    (0::CInt)

tLerp scale t maxT = ((round $ t*scale)::CInt) `mod` (fromInteger maxT)

doubleDraw_ offset spr scr pos
     = drawImage spr scr pos
    >> drawImage spr scr (pos-offset)
doubleDraw = doubleDraw_ $ SDL.V2 ((fromInteger windowWidth) :: CInt) (0::CInt)
doubleDrawDiag :: SDL.Surface -> SDL.Surface -> SDL.V2 CInt
               -> IO (Maybe (SDL.Rectangle CInt))
doubleDrawDiag spr scr pos = do
    doDraw pos
    doDraw (pos-hOffset)
    doDraw (pos-vOffset)
    doDraw (pos-dOffset)
  where
    doDraw = drawImage spr scr
    hOffset = SDL.V2 ((fromInteger windowWidth)  :: CInt) (0::CInt)
    vOffset = SDL.V2 (0::CInt) ((fromInteger windowHeight) :: CInt)
    dOffset = SDL.V2
        ((fromInteger windowWidth)  :: CInt)
        ((fromInteger windowHeight) :: CInt)
